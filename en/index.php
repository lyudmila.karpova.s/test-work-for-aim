<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Demo version of the product 1C-Bitrix: Site Management");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Home page");
?>

	<p> Product <b> 1C-Bitrix: Site Management </b> is a technological core for creating and managing sites, an economical way to develop, support and develop an Internet project. </p>
	<p> The software product <b> 1C-Bitrix: Site Management </b> can be used by both corporate customers and individual developers as a foundation for creating new projects, as well as a tool for managing existing sites. </ p >

	<p> <b> 1C-Bitrix: Site Management </b> allows you to create an unlimited number of sites using one
		copies (licenses) of the product, placing the kernel and the system database in a single copy on the server. </p>
	<p> Also the software product <b> 1C-Bitrix: Site Management </b> allows: </p>

	<ul>
		<li>

			manage the structure and content of the site; </li>

		<li> publish news, press releases and other frequently updated information; </li>
		<li> manage the display of ads on the site; </li>
		<li> create forums; </li>
		<li> send newsletters to groups of subscribers; </li>
		<li> keep track of visit statistics; </li>
		<li> monitor the progress of advertising campaigns; </li>

		<li> implement
			other operations to manage the Internet project. </li>
	</ul>

	<p> The product allows & nbsp; minimize the cost of maintaining a website due to the ease of managing static and dynamic information. To manage your site, you do not need additional services of specialists in the field of web design. With the help of the product, the website will be managed by a full-time employee of the company, an ordinary user of a personal computer without special programming and HTML layout skills. </p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>