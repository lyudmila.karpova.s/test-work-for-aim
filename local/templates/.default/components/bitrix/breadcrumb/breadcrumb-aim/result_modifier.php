<?php

/**
 * @global CMain $APPLICATION
 * @var array    $arParams
 * @var array    $arResult
 */

use Bitrix\Main\Localization\Loc;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();

// переводим на языковую версию
$arResult["lang"] = strtoupper($GLOBALS["lang"]);
if($arResult["lang"] != strtoupper(Aim\Events::$availableLanguagesList[0])){
    $arResult["NAME"] = $arResult["PROPERTIES"]["NAME_".$arResult["lang"]];
    $arResult["DETAIL_TEXT"] = $arResult["PROPERTIES"]["DETAIL_TEXT_".$arResult["lang"]];
    $arResult["PREVIEW_TEXT"] = $arResult["PROPERTIES"]["PREVIEW_TEXT_".$arResult["lang"]];
}
