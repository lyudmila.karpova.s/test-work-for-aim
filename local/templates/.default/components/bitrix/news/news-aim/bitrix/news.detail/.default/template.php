<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="news-detail">
	<? if ($arResult["NAME"]): ?>
		<h3><?= $arResult["NAME"] ?></h3>
	<? endif; ?>
	<? if ($arResult["DETAIL_TEXT"] <> ''): ?>
		<? echo $arResult["DETAIL_TEXT"]; ?>
	<? else: ?>
		<? echo $arResult["PREVIEW_TEXT"]; ?>
	<? endif ?>
</div>
