<?php

/**
 * @global CMain $APPLICATION
 * @var array    $arParams
 * @var array    $arResult
 */

use Bitrix\Main\Localization\Loc;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();

// языковая версия
$arResult["lang"] = strtoupper($GLOBALS["lang"]);
   foreach($arResult["ITEMS"] as $key => $item){
        if($arResult["lang"] != strtoupper(Aim\Events::$availableLanguagesList[0])){

        $arResult["ITEMS"][$key]["NAME"] = $item["PROPERTIES"]["NAME_".$arResult["lang"]]["~VALUE"];
        $arResult["ITEMS"][$key]["DETAIL_TEXT"] = $item["PROPERTIES"]["DETAIL_TEXT_".$arResult["lang"]]["~VALUE"]["TEXT"];
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = $item["PROPERTIES"]["PREVIEW_TEXT_".$arResult["lang"]]["~VALUE"]["TEXT"];
   }
   if(empty($arResult["ITEMS"][$key]["DETAIL_TEXT"]) && empty($arResult["ITEMS"][$key]["PREVIEW_TEXT"])){
    unset($arResult["ITEMS"][$key]);
   }
}
