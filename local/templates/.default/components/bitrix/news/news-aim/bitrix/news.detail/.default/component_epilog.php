<?
defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();
/**
 * @global CMain $APPLICATION
 * @var array    $arParams
 * @var array    $arResult
 */

// меняем заголовок
$APPLICATION->SetPageProperty('title', $arResult["NAME"]);

// добавляем последний пункт в языковой версии
$APPLICATION->AddChainItem($arResult["NAME"]);

// делаем редирект кастомного компонента если новости на текущей страницы нет
if(empty($arResult["DETAIL_TEXT"]) && empty($arResult["PREVIEW_TEXT"])){

	list($lang, $url) = Aim\Events::getCurLang($APPLICATION->GetCurPage());

	if($lang == Aim\Events::$availableLanguagesList[0]){
		$urlRedirect = "/";
	}else{
		$urlRedirect = "/".$lang."/";
	}

	LocalRedirect($urlRedirect);
}
// перекидываем в detail.php
$GLOBALS["ID"] = $arResult["ID"];