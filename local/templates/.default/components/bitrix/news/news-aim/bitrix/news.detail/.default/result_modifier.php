<?php

/**
 * @global CMain $APPLICATION
 * @var array    $arParams
 * @var array    $arResult
 */

use Bitrix\Main\Localization\Loc;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();

// языковая версия
$arResult["lang"] = strtoupper($GLOBALS["lang"]);

if($arResult["lang"] != strtoupper(Aim\Events::$availableLanguagesList[0])){
    $arResult["NAME"] = $arResult["PROPERTIES"]["NAME_".$arResult["lang"]][VALUE];
    $arResult["DETAIL_TEXT"] = $arResult["PROPERTIES"]["DETAIL_TEXT_".$arResult["lang"]][VALUE][TEXT];
    $arResult["PREVIEW_TEXT"] = $arResult["PROPERTIES"]["PREVIEW_TEXT_".$arResult["lang"]][VALUE][TEXT];
}

// перекидываем необходимые данные в component_epilog
$cp = $this->__component;
if (is_object($cp))
{
    $cp->arResult['DETAIL_TEXT'] = $arResult["DETAIL_TEXT"];
    $cp->arResult['PREVIEW_TEXT'] = $arResult["PREVIEW_TEXT"];
    $cp->arResult['ID'] = $arResult["ID"];
    $cp->SetResultCacheKeys(array('DETAIL_TEXT','PREVIEW_TEXT','ID'));
}

