<?php
namespace Seo\Functions;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/lib/template/functions/fabric.php");

class get_case extends \Bitrix\Iblock\Template\Functions\FunctionBase

{

	public static function client()
	{
		$client = new \Bitrix\Main\Web\HttpClient();
		$client->setHeader("Content-Type", "application/json");
		$client->setHeader("Accept", "application/json");

		return $client;
	}

	public function calculate(array $parameters)
	{
		// разбиваем параметры
		$result = $this->parametersToArray($parameters);
		$query = $result[0];

		// смотрим в кеше
		$cache = \Bitrix\Main\Data\Cache::createInstance();
		// кеш на день
		if ($cache->initCache(24*60*60, $query, "/declination/")) {

			$declination = $cache->getVars();

		} elseif ($cache->startDataCache()) {

			$url = "https://ws3.morpher.ru/russian/declension?format=json&s=" .urlencode($query);
			// делаем запрос
			$response = self::client()->get($url);
			$arResponse = json_decode($response, true);
			$declinationResponse = $arResponse;
			if (empty($declinationResponse)) {

				$cache->abortDataCache();

			} else {

				$casesResponse = [
					"Р"             => "rod",
					"Д"             => "dat",
					"В"             => "vin",
					"Т"             => "tv",
					"П"             => "pr",
				];

				foreach ($declinationResponse as $decl => $word) {
					$declination[$casesResponse[$decl]] = $word;
				}
			}

			$cache->endDataCache($declination);
		}

		return $declination[$result[1]];

	}

}
