<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/constants.php')) {
	require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/constants.php');
}

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/events.php')) {
	require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/events.php');
}

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/seo.php')) {
	require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/seo.php');
}

// Определим язык
list($lang, $url) = Aim\Events::getCurLang($APPLICATION->GetCurPage());

$eventManager = \Bitrix\Main\EventManager::getInstance();

// если страница ведет на 404 ошибку, то перекидываем на главную страницу текущей языковой версии
//$eventManager->addEventHandler("main", "OnEpilog", ["Aim\\Events", "redirect404"]);

//исправляем внешние ссылки
$eventManager->addEventHandler("main", "OnEndBufferContent", ["Aim\\Events", "ChangeLink"]);

$eventManager->addEventHandler("iblock","OnTemplateGetFunctionClass", ["Aim\\Events", "getDeclinationClass"]);

/* Функция формирует html код для переключения языковых версий
* по хорошему ее нужно вынести в компонент, но пока так
*/
function getLangHtml(){
	global  $url;
	$html = [];
	foreach( Aim\Events::$availableLanguagesList as $key => $langTmp){
		$urlTmp = '';
		if($key>0){
			$urlTmp = "/".$langTmp;
		}
		$urlTmp .= $url;
		$html[] = '<a  href="'.$urlTmp.'">'.$langTmp.'</a>';
	}

	return implode('&nbsp;/&nbsp;', $html);
}
