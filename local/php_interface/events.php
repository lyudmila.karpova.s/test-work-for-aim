<?php


namespace Aim;

use Bitrix\Main\Context;

class Events
{
	/**
	 * Список доступных языков, используемых для обработчика редиректов страницы 404
	 * по умолчанию на сайте 1 ый в этом массиве язык
	 * @var string[]
	 */
	public static $availableLanguagesList = [
		"ru",
		"en",
	];

	/* Разбирает адрес, возвращает язык и относительный адрес */
	public static function getCurLang($curDir)
	{
		global $APPLICATION;
		$curDir = trim($curDir, '/');
		$langTmp = strstr($curDir, '/', true);

		if (in_array($langTmp, static::$availableLanguagesList)) {
			$lang = $langTmp;
			$url = strstr($curDir, '/') . "/";
		} else {
			$lang = static::$availableLanguagesList[0];
			$url = "/" . $curDir . "/";
		}
		return [$lang, $url];
	}

	/* Редирект при переключении языковых версий */
	public static function redirect404()
	{
		global $APPLICATION, $USER;

		$request = Context::getCurrent()->getRequest();
		$curDir = $APPLICATION->GetCurDir();

		if ($request->isAdminSection()) return;

		list($lang, $url) = static::getCurLang($curDir);

		if (defined("ERROR_404")) {
			// редирект на главную
			LocalRedirect(($lang == static::$availableLanguagesList[0]? "/":"/".$lang."/"));
			return;
		} else {
				$APPLICATION->RestartBuffer();
				/** @noinspection PhpIncludeInspection */
				include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php";
				if (file_exists($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "404.php")) {
					/** @noinspection PhpIncludeInspection */
					include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "404.php";
				}
				/** @noinspection PhpIncludeInspection */
				include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/footer.php";

				return;
			}
	}

	/* Исправляет ссылки на странице */
	public static function ChangeLink(&$content)
	{
		$request = Context::getCurrent()->getRequest();
		if ($request->isAdminSection()) return;
		$doc = new \DOMDocument;
		$doc->loadHTML($content); // Load your HTML

		$xpath = new \DOMXPath($doc);
		$links = $xpath->query('//a[starts-with(@href, "http")]');

		foreach ($links as $link) {
			$link->setAttribute('target', '_blank');
			if ($link->hasAttribute('rel')) {
				$arRel = explode(' ', $link->getAttribute('rel'));
				if (!in_array('noopener', $arRel)) {
					$arRel[] = 'noopener';
				}
				if (!in_array('nofollow', $arRel)) {
					$arRel[] = 'nofollow';
				}
				$link->setAttribute('rel', implode(' ', $arRel));
			} else {
				$link->setAttribute('rel', 'noopener nofollow');
			}
		}


		$content = $doc->saveHTML();
	}

	/* Склоняет слово */
	public static function getDeclinationClass(\Bitrix\Main\Event $event)
	{
		$arParams = $event->getParameters();
		$functionName = $arParams[0];
		switch ($functionName)
		{
			case 'get_case':
				$result = new \Bitrix\Main\EventResult(1,'Seo\Functions\get_case');
				break;
		}
		return $result;
	}

}